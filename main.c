/*

  C Program
  Copyright (C) 2018 R.J.Middleton
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  File Name ......................... main.c
  Written By ........................ Dick Middleton
  Date .............................. 18-Aug-18

  Description : esp32 esp_idf example program to illustrate how to use 
  hwcrypt library functions to encrypt arbitrary data using AES_CBC.

  Last-modified: 2018-08-20  14:35:35 on penguin.lingbrae"

*/
#include <stdio.h>
#include <string.h>
#include <crypto/random.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

#include "hwcrypto/aes.h"

#define delay(ms) (vTaskDelay(ms/portTICK_RATE_MS))
static const char *TSTTAG = "[TESTY]";

// A nice long meaningful string
char testy_str[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed\n"
  "do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n"
  "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris\n"
  "nisi ut aliquip ex ea commodo consequat.  So there!";

#define STRSZ (sizeof(testy_str))

// A structure of things packed to avoid wasted space.  Important for transmission
typedef struct __attribute__((__packed__)) {
  float  floaty_val;
  uint16_t inty_val;
  uint8_t byty_val;
  char stringy_val[STRSZ];
} testy_t;


#define AES_KEY_SIZE 256
#define BLOCKSZ 16

// Secret key - random bytes.  16 Bytes for AES128, 32 Bytes for AES256
uint8_t secret_key[32] = {
  0x12, 0xac, 0xd5, 0x54, 0x24, 0x03, 0x24, 0x06,
  0x21, 0x66, 0xc6, 0xf3, 0x96, 0xe2, 0xba, 0xb3,
  0x80, 0x47, 0x01, 0x2b, 0x23, 0x8f, 0x93, 0xf6,
  0x60, 0x91, 0xb4, 0x64, 0xd1, 0xf0, 0xc7, 0x75,
};


#define DATASZ (sizeof(testy_t))             // bytes in structure
#define MSGBLKS ((DATASZ + BLOCKSZ)/BLOCKSZ) // 16 byte blocks in structure
#define MSGSZ  (BLOCKSZ * MSGBLKS)           // length of data to encrypt
#define BUFRSZ (BLOCKSZ + MSGSZ)             // total length incl IV


void app_main(void) {

  uint8_t crypted[MSGSZ];
  uint8_t decrypted[MSGSZ];
  uint8_t iv[BLOCKSZ];
  
  esp_aes_context ctx;

  delay(1500);  // time to connect monitor
  
  ESP_LOGI(TSTTAG, "Preamble");

  // Copy random bytes to IV
  for (int i = 0; i < 4; i++) {  // where 4 is sizeof uint32_t
    uint32_t wd = esp_random();
    memcpy(iv + (i*4), (uint8_t*) &wd, 4);
  }

  printf("=============== All The Info ===============\n");
  printf("IV: ");  // Just to make sure it worked!
  for (int i = 0; i < BLOCKSZ; i++) {
    printf("%02x ", iv[i]);
  }
  printf("\n");

  printf("testy_t: %d\n", sizeof(testy_t));
  printf("double: %d\n", sizeof(double));
  printf("float: %d\n", sizeof(float));
  printf("int: %d\n", sizeof(int));

  printf("msgblks= %d, msgsz= %d, blksz= %d\n", MSGBLKS, MSGSZ, BLOCKSZ);
  printf("============================================\n\n");

  ESP_LOGI(TSTTAG, "Starting");

  testy_t testy_tx;

  // Arbitrary data values
  testy_tx.floaty_val = 13.13;
  testy_tx.inty_val = 55;
  testy_tx.byty_val = 1;
  memcpy(testy_tx.stringy_val, testy_str, STRSZ);
  
  while(1) {

    //================================================== Encrypt a buffer

    uint8_t src[MSGSZ];
    uint8_t bufr[BUFRSZ];
    memcpy(bufr, iv, BLOCKSZ);
    memcpy(src, &testy_tx, DATASZ);
    
    esp_aes_init(&ctx);
    esp_aes_setkey(&ctx, secret_key, AES_KEY_SIZE );

    if (esp_aes_crypt_cbc(&ctx,
			  ESP_AES_ENCRYPT,
			  MSGSZ,
			  iv,      // this function changes IV
			  src, 
			  crypted) == 0) {
      
      memcpy(bufr+16, crypted, MSGSZ);

      printf("Encrypted: ");
      for (int i = 0; i < MSGSZ; i++) {
	printf("%02x ", crypted[i]);
      }
      printf("\n\n");
    
    }
    else {
      ESP_LOGE(TSTTAG, "encrypt error");
    }
    esp_aes_free(&ctx);

    //================================================== bufr now contains IV and cipher text

    delay(2500);   //   transmit bufr somewhere

    //================================================== Decrypt  bufr

    testy_t testy_rx;  // somewhere to put received data
    
    esp_aes_init(&ctx);
    esp_aes_setkey(&ctx, secret_key, AES_KEY_SIZE );  // <-- same key as encrypt

    uint8_t iv2[BLOCKSZ];
    memcpy(iv2, bufr, BLOCKSZ); // get IV from bufr
    if (esp_aes_crypt_cbc(&ctx,
			  ESP_AES_DECRYPT,
			  MSGSZ,
			  iv2,
			  bufr+BLOCKSZ,
			  decrypted
			  ) == 0) {

      memcpy(&testy_rx, decrypted, DATASZ);   // copy data to structure
    
      printf("Decrypted: ");
      for (int i = 0; i < MSGSZ; i++) {
	printf("%02x ", decrypted[i]);
      }
    
      printf("\nF=%f I=%d  B=%d\n", testy_rx.floaty_val, testy_rx.inty_val, testy_rx.byty_val);
      printf("%s\n\n", testy_rx.stringy_val);

      delay(5000);                  // Then do it all again
      testy_tx.inty_val++;
      testy_tx.floaty_val += 0.01;
    }
    else {
      ESP_LOGE(TSTTAG, "decrypt error");
    }
    esp_aes_free(&ctx);
  }
}

// Local Variables:
// mode:C
// time-stamp-pattern: "30/Last-modified:[ \t]+%:y-%02m-%02d  %02H:%02M:%02S on %h"
// End:
//                           ===//=== */
